(ns riemann.user-test
  "Userland macros for testing snippets of Riemann config."
  (:require (riemann [core]
                     [config]
                     [index]
                     [query])))

(defmacro configure-core
  "Load the given Riemann conf into the current core and reset the index.
FOR TESTING PURPOSES ONLY."
[& conf]
  `(binding [*ns* (find-ns 'riemann.config)]
    (eval '(do
              (~'riemann.time/reset-tasks!)
              (~'clear!)
              (~'pubsub/sweep! (:pubsub @~'core))
              (~'logging/init)
              (~'instrumentation {:enabled? false})
              (~'periodically-expire)
              (~'streams ~@conf)
              (when-let [idx# (:index @~'core)]
                (~'riemann.index/clear idx#))
              (~'apply!)))))

(defn stream-events
  "Run the given events through the current config."
  [& events]
  (doseq [ev events]
    (riemann.core/stream! @riemann.config/core ev)))

(defn search-index
  "Query the current state of the Riemann index."
  [query]
  (riemann.index/search (:index @riemann.config/core)
                        (riemann.query/ast query)))
